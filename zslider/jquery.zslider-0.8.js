/*! TODO дописать документацию
 * Слайдер фотографий или других элементов 
 * с возможностью автоматической прокрутки и автоматического изменения размера
 *
 *********************************************************************
 *	СОБЫТИЯ
 *********************************************************************
 *	- slidechange
 *		Срабатывает после смены слайда. 
 *		В обработчик передается событие в свойстве 'slide', которого хранится номер слайда, который будет показан.
 *
 *  - beforeslidechange
 * 		Срабатывает до смены слайда.
 *		В этот момент смену слайдов можно отменить, для этого у объекта события нужно вызвать метод preventDefault;
 *		Если нужно выполнить какие-то действия перед сменой слайда, то нужно использовать событие slidechanging, так как при beforeslidechange еще нет уверенности, что смена слайда точно произойдет.
 *		В обработчик передается событие в свойстве 'slide', которого хранится номер слайда, который будет показан.
 *
 *  - slidechanging
 * 		Срабатывает при непосредственной смене слайдов. 
 *		В обработчик передается событие в свойстве 'slide', которого хранится номер показанного слайда.
 *
 *  - sliderdestroy
 * 		Срабатывает при отключении слайдера.
 *
 *********************************************************************
 *	МЕТОДЫ
 *********************************************************************
 *	- init
 *		Инициализация плагина.
 *		Сюда передаются начальные параметры слайдера.
 *
 *	- destroy
 *		Уничтожение плагина.
 *
 *	- getCurrentSlideIndex
 *		Получение номера текущего слайда. Номера начинаются с нуля.
 *
 *	- setCurrentSlide
 *		Перемещение слайдера в определенную позицию.
 *		Дополнительно передается номер слайда виде числа. Номерация начинается с нуля.
 *
 *  - setCurSlideWithoutAnimation
 *		Перемещение слайдера в определенную позицию без анимации.
 *		Дополнительно передается номер слайда виде числа. Номерация начинается с нуля.
 *
 *	- isSlideShown
 *		Определяет, показан ли слайд в текущий момент.
 *		Дополнительно передается номер слайда виде числа. Номерация начинается с нуля.
 *
 *	- resize
 *		Пересчет размеров слайдов
 *
 *  - play 
 *		Запускает слайд-шоу
 *
 *  - pause
 *		Останавливает слайд-шоу
 *
 */
;(function( $, window, document, undefined ){
	
	//prevent multiple initialization
    if (window.jqueryZsliderInitialized !== undefined) {
        return;
    }
	window.jqueryZsliderInitialized = true;
	
	function inherit(proto) {
		function F() {};
		F.prototype = proto;
		var object = new F;
		return object;
	}
	if (!Object.create) Object.create = inherit;
	
	var defaultSettings = {
	  'isVertical':				false, //признак, что слайдер вертикальный
	  'shownSlidesCount':		1, //количество одновременно показываемых слайдов, по умолчанию - 1
	  'slidesMargin': 			0, //расстояние между слайдами
	  'circularScrolling':		true, //признак того, что нужна круговая прокрутка, по умолчанию - true
	  'sliderInterval': 		5000, //интервал между автоматической прокруткой, по умолчанию - 5000, если установлено в 0, то не будет автоматической прокрутки
	  'intervalMoveCount':	undefined, //количество слайдов, на которое будет производиться сдвиг по таймеру (если не задано, используется значение sliderMoveCount)
	  'goPrevLink': 			null, //идентификатор кнопки для прокрутки слайдера влево
	  'goNextLink': 			null, //идентификатор кнопки для прокрутки слайдера вправо
	  'inactiveLinksClass': 	'inactive', //класс, который присваивается стрелкам влево и вправо, если достугнут конец списка, по умолчанию - inactive
	  'slidesArrowsMoveCount':	undefined, //количество слайдов, на которое будет производиться сдвиг при клике на стрелки (если не задано, используется значение sliderMoveCount)
	  'sliderSpeed': 			300, //время прокрутки в милисекундах, по умолчанию
	  'slidesLinks': 			null, //идентификатор списка ссылок на слайды, например, номера страниц (используется синтаксис jquery, например, ".className a")
	  'slidesLinksSelectedClass': 'selected', //класс, который присваивается выбранным ссылкам на слайды (номера страниц)
	  'slidesLinksChangeClassEvent': 'slidechanging', //событие, при котором будет происходить смена класса у ссылок (это должны быть события слайдера)
	  'slidesLinksOut': 		false, //признак того, что переключаться между слайдами нужно не при клике на номер страницы, а при наведении, по умолчанию - false
	  'slidesLinksMoveCount':	undefined, //количество слайдов, на которое будет производиться сдвиг при клике на ссылки (если не задано, используется значение sliderMoveCount)
	  'showSlideAnimation': 	"scroll", //тип анимации при клике на номер страницы, 
							  /*
							   * "" - без анимации 
							   * "fadeOut" - исчезание
							   * "scroll" - с покруткой
							   * "overLap" - перекрытие
							   * "customSlide" - пользовательская
							   * "customSlideOverflow" - пользовательская, при которой слайд будет виден за пределами слайдера
							   */
	  'showEmptyPlaces':		true, //нужно ли показывать пустые места в конце слайдера, если одновременно может быть показано несколько слайдов
	  'animationSettings':		null, //дополнительные параметры анимации
	  'sliderMoveCount':		1, //количество слайдов, на которое будет производиться сдвиг
	  'stretchSlideIfNoEnoughCount':		false, //растягивать слайды на всю весь размер слайдера, если слайдов меньше, чем нужно
	  'allowTouchScroll': true //включает прокрутку слайдера пальцами
	};
	
	//открытые методы плагина
	var methods = {
		
		init: function(options){ //инициализация плагина
			
			return this.each(function(){
				
				var $this = $(this);
				
				if( typeof slidersStorage.get($this) === "undefined" ){
					
					var settings = $.extend(true, {}, $.fn.zslider.defaults, options);
					
					var sliderStorageItem = {};
					
					if(settings.showEmptyPlaces){
						//add invisible slides
						var invisibleSlidesController = new AdditionalSlidesController(
							$this.children("ul").eq(0),
							settings.shownSlidesCount,
							settings.sliderMoveCount
						);
						sliderStorageItem.invisibleSlides = invisibleSlidesController;
					}
					
					//create a slider
					var slideChanger = buildSliderChanger(settings);
					var sliderSettings = {
						'slideChanger': slideChanger,
						'shownSlidesCount': settings.shownSlidesCount,
						'slidesMargin': settings.slidesMargin,
						'isVertical': settings.isVertical,
						'stretchSlideIfNoEnoughCount': settings.stretchSlideIfNoEnoughCount
					};
					
					var slider;
					if(settings.circularScrolling){
						slider = new CircularSlider($this, sliderSettings);
					} else {
						slider = new Slider($this, sliderSettings);
					}
					
					//save the Slider object
					sliderStorageItem.slider = slider;
					
					//create a animation controller
					var sliderController = new SliderAnimationController(slider);
					sliderStorageItem.sliderContainer = sliderController;
					
					//events holder
					var handlersStorage = new EventHandlersStorage();
					//set slide links settings
					if(settings.slidesLinks){
						var linksMoveCount = settings.slidesLinksMoveCount !== undefined ? settings.slidesLinksMoveCount : settings.sliderMoveCount;
						var linkSettings = {
							'slidesLinks': settings.slidesLinks,
							'slidesLinksSelectedClass': settings.slidesLinksSelectedClass,
							'slidesLinksOut': settings.slidesLinksOut,
							'showSlideFunction': function(slideIndex){
								sliderController.showSlide(slider.getValidIndex(slideIndex * linksMoveCount));
							}
						};
						var links = new SlideLinks(linkSettings);
						handlersStorage.add(
							$this,
							settings.slidesLinksChangeClassEvent,
							function(e){
								links.highlightLink(Math.ceil(e.slide / linksMoveCount));
							}
						);
						
						sliderStorageItem.links = links;
					}
					
					//arrows
					var arrowsMoveCount = settings.slidesArrowsMoveCount !== undefined ? settings.slidesArrowsMoveCount : settings.sliderMoveCount;
					//set prev arrow
					if(settings.goPrevLink){
						var prevArrowSettings = {
							'arrow': settings.goPrevLink, 
							'inactiveClass': settings.inactiveLinksClass,
							'onClick': function(e){
								sliderController.showPrev(arrowsMoveCount);
							}
						}
						var prevArrowLink = new ArrowLink(prevArrowSettings);
						handlersStorage.add(
							$this,
							'slidechange',
							function(e){
								prevArrowLink.setActive(sliderController.getSlider().hasPrev(1));
							}
						);
						prevArrowLink.setActive(sliderController.getSlider().hasPrev(1));
						
						sliderStorageItem.prevArrowLink = prevArrowLink;
					}
					//set next arrow
					if(settings.goNextLink){
						var nextArrowSettings = {
							'arrow': settings.goNextLink, 
							'inactiveClass': settings.inactiveLinksClass,
							'onClick': function(e){
								sliderController.showNext(arrowsMoveCount);
							}
						}
						var nextArrowLink = new ArrowLink(nextArrowSettings);
						handlersStorage.add(
							$this,
							'slidechange',
							function(e){
								nextArrowLink.setActive(sliderController.getSlider().hasNext(1));
							}
						);
						nextArrowLink.setActive(sliderController.getSlider().hasNext(1));
						
						sliderStorageItem.nextArrowLink = nextArrowLink;
					}
					
					//enable showing next slides after a time interval
					if(settings.sliderInterval > 0){
						var timerMoveCount = settings.intervalMoveCount !== undefined ? settings.intervalMoveCount : settings.sliderMoveCount;
						var timer = new Timer(
							settings.sliderInterval, 
							function (){
								sliderController.showNext(timerMoveCount);
							}
						);
						handlersStorage.add(
							$this,
							'slidechange',
							function(e){
								timer.start();
							}
						);
						handlersStorage.add(
							$this,
							'slidechanging',
							function(e){
								timer.stop();
							}
						);
						timer.start();
						sliderStorageItem.timer = timer;
					}
					
					//enable resizing after the windows resize
					handlersStorage.add(
						window,
						'resize',
						function(){ sliderController.resizeSlides(); }
					);
					
					if(settings.allowTouchScroll){
						var touchControllerParams = {};
						if(settings.isVertical){
							touchControllerParams.allowHorizontalMoving = false;
						} else {
							touchControllerParams.allowVerticalMoving = false;
						}
						var list = $this.children("ul").eq(0);
						var touchController = new MoveTouchController(list, touchControllerParams);
						handlersStorage.add(
							list,
							'touchControllerStart',
							function(e){
								if(!sliderController.tryDisable()){
									e.preventDefault();
								}
							}
						);
						handlersStorage.add(
							list,
							'touchControllerStop',
							function(e, data){
								//calc new slide index
								
								var acceler = 4000; // px/s`2
								var speed;
								var currentPosition;
								var animationCssStyle;
								if(settings.isVertical){
									speed = data.speedY;
									currentPosition = data.positionY;
									animationCssStyle = "margin-top";
								} else {
									speed = data.speedX;
									currentPosition = data.positionX;
									animationCssStyle = "margin-left";
								}
								
								var timeToStop = speed * 1000 / acceler;
								var distance = acceler * timeToStop * Math.abs(timeToStop) / 2;
								var newSlideIndex = slider.getSlideByPosition(currentPosition - distance);
								var slidesChange = slider.getCurrentSlide() - newSlideIndex;
								slidesChange = Math.round(slidesChange / settings.sliderMoveCount) * settings.sliderMoveCount;
								newSlideIndex = slider.getCurrentSlide() - slidesChange;
								if(Math.abs(slidesChange) > settings.shownSlidesCount){
									if(slidesChange > 0){
										slidesChange = Math.floor(settings.shownSlidesCount / settings.sliderMoveCount) * settings.sliderMoveCount;
									} else {
										slidesChange = -1 * Math.floor(settings.shownSlidesCount / settings.sliderMoveCount) * settings.sliderMoveCount;
									}
									newSlideIndex = slider.getCurrentSlide() - slidesChange;
								}
								var isNewIndexAcceptable;
								if(slidesChange > 0){
									isNewIndexAcceptable = slider.hasPrev(slidesChange);
								} else {
									isNewIndexAcceptable = slider.hasNext(Math.abs(slidesChange));
								}
								var newRealIndex = slider.getValidIndex(newSlideIndex);
								
								//move to new slide
								var newPosition;
								if(isNewIndexAcceptable){
									newPosition = slider.getPosition(newSlideIndex);
								} else {
									newPosition = slider.getPosition(newRealIndex);
								}
								
								var easing = "zEasing";
								var animatonTime = (currentPosition - newPosition) * 2 / speed;
								if(timeToStop == 0 || animatonTime < 0){
									easing = "swing";
									animatonTime = 1000;
								}
								
								var animationParams = {};
								animationParams[animationCssStyle] = newPosition;
								list.animate(animationParams, animatonTime, easing, function(){
									slider.setCurrentSlide(newRealIndex);
									sliderController.enable();
								});
							}
						);
						
						sliderStorageItem.touchController = touchController;
					}
					
					//save event handlers
					sliderStorageItem.handlersStorage = handlersStorage;
					//save all objects 
					slidersStorage.add($this, sliderStorageItem);
				}
			});
		},
		destroy: function(options){ //уничтожение плагина
			
			return this.each(function(){
				//берем текущий элемент и данные плагина для этого элемента
				var $this = $(this);
				var sliderStorageItem = slidersStorage.get($this);
				
				if( typeof sliderStorageItem !== "undefined" ){
				
					var keys = ["invisibleSlides", "slider", "sliderContainer", "links", "prevArrowLink", "nextArrowLink", "timer", "touchController", "handlersStorage"];
					
					var storageValue;
					for(var i = keys.length - 1; i >= 0; i--){
						storageValue = sliderStorageItem[keys[i]];
						if( typeof storageValue !== "undefined" ){
							storageValue.destroy()
						}
					}
					slidersStorage.add($this, undefined);
				}
			});
		},
		getCurrentSlideIndex: function(options){//получение номера текущего слайда
			//берем текущий элемент и данные плагина для этого элемента
			var $this = $(this).eq(0);
			var sliderStorageItem = slidersStorage.get($this);
			
			if( typeof sliderStorageItem !== "undefined" ){
				return sliderStorageItem.slider.getCurrentSlide();
			} else {
				$.error( 'zslider/getCurrentSlideIndex: zslider is not initialized.' );
			}
		},
		setCurrentSlide: function(index){ //устанавливаем слайдер на определенный слайд
			
			return this.each(function(){
			
				var slideIndex = parseInt(index);
				
				if(isNaN(slideIndex)){
					$.error( 'zslider/setCurrentSlide: Incorrect value of index.' );
				} else {
					//берем текущий элемент и данные плагина для этого элемента
					var $this = $(this);
					var sliderStorageItem = slidersStorage.get($this);
					
					if( typeof sliderStorageItem !== "undefined" ){
						sliderStorageItem.sliderContainer.showSlide(slideIndex);
					} else {
						$.error( 'zslider/setCurrentSlide: zslider is not initialized.' );
					}
				}
				
			});
		},
		setCurSlideWithoutAnimation: function(index){ //устанавливаем слайдер на определенный слайд без анимации
			
			return this.each(function(){
			
				var slideIndex = parseInt(index);
				
				if(isNaN(slideIndex)){
					$.error( 'zslider/setCurSlideWithoutAnimation: Incorrect value of index.' );
				} else {
				
					//берем текущий элемент и данные плагина для этого элемента
					var $this = $(this);
					var sliderStorageItem = slidersStorage.get($this);
					
					if( typeof sliderStorageItem !== "undefined" ){
						sliderStorageItem.sliderContainer.setCurrentSlide(slideIndex);
					} else {
						$.error( 'zslider/setCurSlideWithoutAnimation: zslider is not initialized.' );
					}
				}
				
			});
		},
		isSlideShown: function(index){//показан ли текущий слайд
			
			var slideIndex = parseInt(index);
				
			if(isNaN(slideIndex)){
				$.error( 'zslider/isSlideShown: Incorrect value of index.' );
			} else {
				//берем текущий элемент и данные плагина для этого элемента
				var $this = $(this).eq(0);
				var sliderStorageItem = slidersStorage.get($this);
					
				if( typeof sliderStorageItem !== "undefined" ){
					return sliderStorageItem.slider.isSlideShown(slideIndex);
				} else {
					$.error( 'zslider/isSlideShown: zslider is not initialized.' );
				}
			}
			
		},
		resize: function(){ //пересчет размеров
			
			return this.each(function(){
				
				//берем текущий элемент и данные плагина для этого элемента
				var $this = $(this);
				var sliderStorageItem = slidersStorage.get($this);
				
				if( typeof sliderStorageItem !== "undefined" ){
					sliderStorageItem.sliderContainer.resizeSlides();
				} else {
					$.error( 'zslider/resize: zslider is not initialized.' );
				}				
			});
		},
		play: function(){ //запуск слайд-шоу
			
			return this.each(function(){
				
				//берем текущий элемент и данные плагина для этого элемента
				var $this = $(this);
				var sliderStorageItem = slidersStorage.get($this);
				
				if( typeof sliderStorageItem !== "undefined" ){
					sliderStorageItem.timer.on();
				} else {
					$.error( 'zslider/play: zslider is not initialized.' );
				}				
			});
		},
		pause: function(){ //пауза слайд-шоу
			
			return this.each(function(){
				
				//берем текущий элемент и данные плагина для этого элемента
				var $this = $(this);
				var sliderStorageItem = slidersStorage.get($this);
				
				if( typeof sliderStorageItem !== "undefined" ){
					sliderStorageItem.timer.off();
				} else {
					$.error( 'zslider/pause: zslider is not initialized.' );
				}				
			});
		}
	};
	
	$.fn.zslider = function(method){
		
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if( (typeof method === 'object') || (! method)){
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Метод с именем ' +  method + ' не существует для jQuery.zslider' );
		}
		
	};
	
	//параметры плагина по умолчанию
	$.fn.zslider.defaults = defaultSettings;
	
	$.fn.zslider.animations = {
		"": {
			type: "list",
			animation: NoListAnimation
		}, 
		"scroll": {
			type: "jumplist",
			animation: JqueryListAnimation,
			defaults: {
				easing: "swing"
			}
		},
		"fadeOut": {
			type: "slide",
			animation: SlideBySlideAnimation,
			settings: {
				prevSlideAnimation: function(elem, duration, endFunction){
					endFunction();
				},
				nextSlideAnimation: function(elem, duration, endFunction){
					elem.fadeIn(duration, endFunction);
				},
				prevNextInterval: 0
			},
			defaults: {
				slideInterval: 0
			}
		},
		"overLap": {
			type: "slide",
			animation: SlideBySlideAnimation,
			settings: {
				prevSlideAnimation: function(elem, duration, endFunction){
					endFunction();
				},
				nextSlideAnimation: function(elem, duration, endFunction){
					elem.show();
					var endPosition = elem.position();
					elem.css({left: endPosition.left + elem.parent().parent().width() + 1});
					elem.animate(endPosition, duration, endFunction);
				},
				prevNextInterval: 0
			},
			defaults: {
				slideInterval: 0
			}
		},
		"customSlide":{
			type: "slide",
			animation: SlideBySlideAnimation,
			defaults: {
				slideInterval: 0,
				prevNextInterval: 0,
				prevSlideAnimation: function(elem, duration, endFunction){
					endFunction();
				},
				nextSlideAnimation: function(elem, duration, endFunction){
					endFunction();
				}
			}
		},
		"customSlideOverflow":{
			type: "slideOverflow",
			animation: SlideBySlideAnimation,
			defaults: {
				slideInterval: 0,
				prevNextInterval: 0,
				prevSlideAnimation: function(elem, duration, endFunction){
					endFunction();
				},
				nextSlideAnimation: function(elem, duration, endFunction){
					endFunction();
				}
			}
		}
	}
	
	function buildSliderChanger(params){
		var animationType = $.fn.zslider.animations[params.showSlideAnimation];
		
		if(typeof animationType === "undefined"){
			$.error( "zslider: a wrong animation type." );
		}
		
		var animationParams = $.extend(true, {}, animationType.defaults, params.animationSettings, animationType.settings);
		if( typeof animationParams.sliderSpeed === "undefined" ){
			animationParams.sliderSpeed = params.sliderSpeed;
		}
		var animation = new animationType.animation(animationParams);
		var slideChangerType = getSlideChangerByType(animationType.type);
		return new slideChangerType(animation);
	}
	
	function getSlideChangerByType(type){
		switch(type){
			case "list":
				return ListSlideChanger;
			case "jumplist":
				return JumpListSlideChanger;
			case "slide":
				return SlideBySlideChanger;
			case "slideOverflow":
				return SlideBySlideOverflowChanger;
			default:
				$.error( "zslider: a wrong slide changer type." );
		}
	}
	
	var slidersStorage = new ObjectStorage("zslider-sliders");
	
	//---------------------------------------- CLASSES ---------------------------------------------
	
	function Slider(parentElement, params){
		this._parentElement = parentElement;
		
		if(this._parentElement.children("ul").length < 1 ){
			$.error( "zslider: the zslider's parent element must contain an 'ul' element." );
		}
		
		this._list = this._parentElement.children("ul").eq(0);
		this._slideChanger = params.slideChanger;
		this._shownCount = params.shownSlidesCount;
		this._paramsShownCount = params.shownSlidesCount;
		this._stretchSlide = params.stretchSlideIfNoEnoughCount;
		this._showEmptyPlaces = params.showEmptyPlaces;
		this._margin = params.slidesMargin;
		this._isVertical = params.isVertical;
		this._count = 0;
		this._slideSize = 1;
		this._slideDisplacement = 1;
		this._sliderSize = 1;
		this._listSize = 1;
		this._currentSlide = 0;
		this._interval = 0;
		this._isMoving = false;
		this._disabled = false;
		this._currentPosition = 0;
		
		if(this._isVertical){
			this._motionCSSParam = 'margin-top';
			this._elementsMarginParam = 'margin-bottom';
			this._getSize = function(elem){ return $(elem).height(); };
			this._setSize = function(elem, size){ $(elem).css("height", size + 'px'); };
			this._clearSize = function(elem){ $(elem).prop('style').removeProperty('height'); };
			this._containerClass = "zslider-container-vertical";
		} else {
			this._motionCSSParam = 'margin-left';
			this._elementsMarginParam = 'margin-right';
			this._getSize = function(elem){ return $(elem).width(); };
			this._setSize = function(elem, size){ $(elem).css("width", size + 'px'); };
			this._clearSize = function(elem){ $(elem).prop('style').removeProperty('width'); };
			this._containerClass = "zslider-container";
		}
		
		this._initHtmlElements();
		this.resizeSlides();
	}
	Slider.prototype.getParent = function(){
		return this._parentElement;
	}
	Slider.prototype.isDisabled = function(){ return this._disabled; };
	Slider.prototype.getCurrentSlide = function(){ return this._currentSlide; };
	Slider.prototype.showSlide = function(index){
		this._beforeSlideChange(index, this._showSlide);
	};
	Slider.prototype.setCurrentSlide = function(index){
		this._beforeSlideChange(index, this._setCurrentSlide);
	};
	Slider.prototype._beforeSlideChange = function(index, changeFunction){
		if(!this._isMotionAvailable(index)){
			return;
		}
		var event = jQuery.Event("beforeslidechange");
		event.slide = index;
		this._parentElement.triggerHandler(event);
		if(!event.isDefaultPrevented()){
			this._parentElement.triggerHandler({
				type: 'slidechanging',
				slide: index
			});
			changeFunction.call(this, index);
		}
	};
	Slider.prototype.resizeSlides = function(){ 
		this._resize();
		this._setCurrentSlide(this._currentSlide); 
	};
	Slider.prototype._initHtmlElements = function(){
		//добавяем класс к списку
		this._list.addClass('zslider-container-in');
		
		//добавляем класс для выбранного элемента
		this._parentElement.addClass(this._containerClass);	

		//сначала определяем потомков
		var slides = this._list.children("li");
		//добавляем им слайдам класс для их дальнейшего определения
		slides.addClass("zslider-slide");
		this._count = slides.length;

		//проверяем количество доступных слайдов, если их меньше, чем нужное количество на одном экране
		//то изменяем количество
		if(this._shownCount >= slides.length){
			this._shownCount = slides.length;
			this._disabled = true;
		}
	};
	Slider.prototype._showSlide = function(index){
		this._showSlideWithChanger(index, this._slideChanger);
	};
	Slider.prototype._showSlideWithChanger = function(index, slideChanger){
		this._onBeforeShowSlide(index);
		var _this = this;
		function _innerAfterShowSlide(){
			_this._onAfterShowSlide(index);
		};
		slideChanger.startChanging({
			slider: this,
			index: index,
			endFunction: _innerAfterShowSlide
		});
	};
	Slider.prototype._onBeforeShowSlide = function(index){
		 this._isMoving = true;
	};
	Slider.prototype._onAfterShowSlide = function(index){
		this._isMoving = false;
		this._setCurrentSlide(index);
	};
	Slider.prototype._resize = function(){
		this._sliderSize = this._getSize(this._parentElement);
		
		//берем слайды
		var slides = this._list.children('.zslider-slide');
		//определяем ширину одного слайда
		if(this._stretchSlide){
			this._slideSize = (this._sliderSize + this._margin) / this._shownCount - this._margin;
		} else {
			this._slideSize = (this._sliderSize + this._margin) / this._paramsShownCount - this._margin;
		}
		this._setSize(slides, this._slideSize);
		slides.css(this._elementsMarginParam, this._margin + 'px');
		//устанавливаем смещение для одного слайда
		this._slideDisplacement = this._slideSize + this._margin;
		//задаем ширину блока со всеми слайдами
		this._listSize = slides.size() * this._slideDisplacement;
		this._setSize(this._list, this._listSize + 1);
	};
	Slider.prototype.destroy = function(){
		this._parentElement.triggerHandler({
			type: 'sliderdestroy'
		});
		if(this._isMoving){
			if(typeof this._slideChanger.destroy !== 'undefined'){
				this._slideChanger.destroy();
			}
		}
		this._destroy();
	}
	Slider.prototype._destroy = function(){
		//берем все слайды
		var slides = this._list.children("li");
		slides.removeClass('zslider-slide');
		
		var $this = this;
		//удаляем стили для слайдов
		slides.each(function(){
			$this._clearSize(this);
			$(this).prop('style').removeProperty($this._elementsMarginParam);
			$(this).prop('style').removeProperty("display");
		});
		
		//удаляем класс у внутреннего контейнера и не созданные стили
		this._list.removeClass("zslider-container-in");
		this._clearSize(this._list);
		this._list.prop('style').removeProperty(this._motionCSSParam);
		
		//удалем класс у текущего элемента
		this._parentElement.removeClass(this._containerClass);
		
		delete this._list;
		delete this._parentElement;
	};
	Slider.prototype._setCurrentSlide = function(index){
		this._moveList(this._getPosition(index));
		if(index != this._currentSlide){
			this._currentSlide = index;
			this._parentElement.triggerHandler({
				type: 'slidechange',
				slide: index
			});
		}
	};
	Slider.prototype._moveList = function(position){
		this._moveAnyList(this._list, position);
		this._currentPosition = position;
	};
	Slider.prototype._moveAnyList = function(list, position){
		list.css(this._motionCSSParam, position);
	};
	Slider.prototype._getPosition = function(index){
		return -1 * index * this._slideDisplacement;
	};
	Slider.prototype.getPosition = function(index){
		if(this._isValidIndex(index)){
			return this._getPosition(index);
		} else {
			return undefined;
		}
	};
	Slider.prototype.getSlideByPosition = function(position){
		return this._getSlideByPosition(position);
	};
	Slider.prototype._getSlideByPosition = function(position){
		return Math.round( -position / this._slideDisplacement );
	};
	Slider.prototype._isMotionAvailable = function(index){
		return !this._isMoving && !this._disabled && (index != this._currentSlide) && this._isValidIndex(index);
	};
	Slider.prototype.isMoving = function(){
		return this._isMoving;
	}
	Slider.prototype.isSlideShown = function(index){
		return this._isSlideShown(index);
	};
	Slider.prototype._isSlideShown = function(checkedIndex, curIndex){
		var currentIndex = curIndex || this._currentSlide;
		return (checkedIndex >= currentIndex) && (checkedIndex < (currentIndex + this._shownCount));
	};
	Slider.prototype._isValidIndex = function(index){
		if(this._showEmptyPlaces){
			return (index >= 0) && (index < this._count);
		} else {
			return (index >= 0) && (index <= this._count - this._shownCount);
		}
	};
	Slider.prototype.lastIndex = function(){
		if(this._showEmptyPlaces){
			return this._count - 1;
		} else {
			return this._count - this._shownCount;
		}
	};
	Slider.prototype.getValidIndex = function(index){
		return this._getValidIndex(index);
	}
	Slider.prototype._getValidIndex = function(index){
		if(index < 0){
			return 0;
		}
		var lastIndex = this.lastIndex();
		if(index > lastIndex){
			return lastIndex;
		}
		return index;
	}
	Slider.prototype.isSlideAvailable = function(index){
		return this._isValidIndex(index);
	};
	Slider.prototype.hasNext = function(count){
		return !this._disabled && this._isValidIndex(this._getNextIndex(count));
	};
	Slider.prototype.hasPrev = function(count){
		return !this._disabled && this._isValidIndex(this._getPrevIndex(count));
	};
	Slider.prototype._getNextIndex = function(count){
			var _count = typeof count !== "undefined" ? count : 1;
		return this._currentSlide + _count;
	};
	Slider.prototype._getPrevIndex = function(count){
		var _count = typeof count !== "undefined" ? count : 1;
		return this._currentSlide - _count;
	};
	Slider.prototype.showNext = function(count){
		this._beforeSlideChange(this.getValidIndex(this._getNextIndex(count)), this._showSlide);
	};
	Slider.prototype.showPrev = function(count){
		this._beforeSlideChange(this.getValidIndex(this._getPrevIndex(count)), this._showSlide);
	};
	Slider.prototype._getSlide = function(index){
		return this._list.children("li").eq(index);
	};
	
	function CircularSlider(parentElement, params){
		Slider.apply(this, arguments);
		this._nextMoving = false;
		this._prevMoving = false;
	}
	CircularSlider.prototype = Object.create(Slider.prototype);
	CircularSlider.prototype.constructor = CircularSlider;
	CircularSlider.prototype._initHtmlElements = function(){
	
		Slider.prototype._initHtmlElements.apply(this, arguments);
		if(this._disabled){
			return;
		}
		
		var primarySlides = this._list.children('.zslider-slide');
		//берем первые элементы
		for(i = 0; i < this._shownCount ; i++){
			primarySlides.eq((i*2) % primarySlides.length).clone(true).addClass("zslider-secondary-slide").appendTo(this._list);
			primarySlides.eq((i*2 + 1) % primarySlides.length).clone(true).addClass("zslider-secondary-slide").appendTo(this._list);
			primarySlides.eq(primarySlides.length - 1 - (i % primarySlides.length)).clone(true).addClass("zslider-secondary-slide").prependTo(this._list);
		}
		//добавляем изначальным слайдам класс для их дальнейшего определения
		primarySlides.addClass("zslider-primary-slide");
	};
	CircularSlider.prototype._destroy = function(){
		this._list.children('.zslider-secondary-slide').remove();
		this._list.children('.zslider-primary-slide').removeClass('zslider-primary-slide');
		Slider.prototype._destroy.apply(this, arguments);
	};
	CircularSlider.prototype._getPosition = function(index){
		if(this._disabled){
			return Slider.prototype._getPosition.call(this, index);
		} else {
			return Slider.prototype._getPosition.call(this, index + this._shownCount);
		}
	};
	CircularSlider.prototype.getPosition = function(index){
		return this._getPosition(index);
	};
	CircularSlider.prototype._getSlideByPosition = function(position){
		return Slider.prototype._getSlideByPosition.call(this, position) - this._shownCount;
	};
	CircularSlider.prototype._isValidIndex = function(index){
		return (index >= 0) && (index <= this._count - 1);
	};
	CircularSlider.prototype.lastIndex = function(){
		return this._count - 1;
	};
	CircularSlider.prototype._onBeforeShowSlide = function(index){
		Slider.prototype._onBeforeShowSlide.apply(this, arguments);
		
		if(this._slideChanger.CircularMovingRequired !== true){
			return;
		}
		
		if((index < this._currentSlide) && (this._isSlideShown(index) || this._nextMoving)){
			//надо переместить в начало
			this._moveList(this._getPosition(this._currentSlide - this._count));
		} else if((index > (this._currentSlide + this._shownCount - 1)) && (this._isSlideShown(this._currentSlide, index) || this._prevMoving)){
			//надо переместить в самый конец
			this._moveList(this._getPosition(this._currentSlide + this._count));
		}
	};
	CircularSlider.prototype._getNextIndex = function(count){
		var next = Slider.prototype._getNextIndex.call(this, (count % this._count));
		if(this._isValidIndex(next)){
			return next;
		} else {
			return next - this._count;
		}
	};
	CircularSlider.prototype._getPrevIndex = function(count){
		var prev = Slider.prototype._getPrevIndex.call(this, (count % this._count));
		if(this._isValidIndex(prev)){
			return prev;
		} else {
			return prev + this._count;
		}
	};
	CircularSlider.prototype.hasNext = function(count){
		return !this._disabled;
	};
	CircularSlider.prototype.hasPrev = function(count){
		return !this._disabled;
	};
	CircularSlider.prototype.showNext = function(count){
		this._nextMoving = true;
		this._prevMoving = false;
		Slider.prototype.showNext.apply(this, arguments);
	};
	CircularSlider.prototype.showPrev = function(count){
		this._nextMoving = false;
		this._prevMoving = true;
		Slider.prototype.showPrev.apply(this, arguments);
	};
	CircularSlider.prototype.showSlide = function(){
		this._nextMoving = false;
		this._prevMoving = false;
		Slider.prototype.showSlide.apply(this, arguments);
	};
	CircularSlider.prototype._isSlideShown = function(index, curIndex){
		var currentIndex = curIndex || this._currentSlide;
		if(!this._isValidIndex(index)){
			return false;
		}
		var additionalCheckIndex = (currentIndex + this._shownCount) - this._count;
		return ((index >= currentIndex) && (index < (currentIndex + this._shownCount))) ||
			(index < additionalCheckIndex);
	};
	CircularSlider.prototype._getSlide = function(index){
		return Slider.prototype._getSlide.call(this, index + this._shownCount);
	};
	CircularSlider.prototype.getValidIndex = function(index){
		if(this._currentSlide < index){
			return this._getNextIndex(index - this._currentSlide);
		} else {
			return this._getPrevIndex(this._currentSlide - index);
		}
	}
	
	function ListSlideChanger(animation){
		this._animation = animation;
		this.CircularMovingRequired = true;
	};
	ListSlideChanger.prototype.startChanging = function(params){
		this._animation.animate(
			params.slider._list,
			params.slider._motionCSSParam,
			this._getEndPosition(params),
			this._getEndFunction(params)
		);
	};
	ListSlideChanger.prototype._getEndPosition = function(params){
		return params.slider._getPosition(params.index);
	};
	ListSlideChanger.prototype._getEndFunction = function(params){
		return params.endFunction;
	};
	ListSlideChanger.prototype.destroy = function(){
		if(typeof this._animation.destroy !== 'undefined'){
			this._animation.destroy();
		}
		if(typeof this._destroy !== 'undefined'){
			this._destroy();
		}
	};
	function JumpListSlideChanger(animation){
		ListSlideChanger.apply(this, arguments);
	};
	JumpListSlideChanger.prototype = Object.create(ListSlideChanger.prototype);
	JumpListSlideChanger.prototype.startChanging = function(params){
		this._slidesHidden = false;
		this._hiddenCount = 0;
		this._needChangeEndPosition = false;
		
		var slidesDif = params.slider.getCurrentSlide() - params.index;
		//проверяем, нужно ли скрывать лишние слайды
		if( ( Math.abs(slidesDif) <= params.slider._shownCount ) ||
		( (slidesDif > 0) && (params.slider._getPosition(params.index) < params.slider._currentPosition) ) ||
		( (slidesDif < 0) && (params.slider._getPosition(params.index) > params.slider._currentPosition) ) ){
			ListSlideChanger.prototype.startChanging.apply(this, arguments);
			return;
		}
		this._slidesHidden = true;
		this._hiddenCount = Math.abs(slidesDif) - params.slider._shownCount;
		var hiddenSlidesStartIndex = Math.min(params.slider.getCurrentSlide(), params.index) + params.slider._shownCount;
		for(var i = 0; i < this._hiddenCount; i++){
			params.slider._getSlide(hiddenSlidesStartIndex + i).addClass("zslider-jump-hidden").hide();
		}
		if(slidesDif > 0){
			params.slider._moveList(params.slider._getPosition(params.slider.getCurrentSlide() - this._hiddenCount));
		} else {
			this._needChangeEndPosition = true;
		}
		ListSlideChanger.prototype.startChanging.apply(this, arguments);
	};
	JumpListSlideChanger.prototype._getEndPosition = function(params){
		if(this._needChangeEndPosition){
			return params.slider._getPosition(params.index - this._hiddenCount);
		} else {
			return ListSlideChanger.prototype._getEndPosition.apply(this, arguments);
		}
	};
	JumpListSlideChanger.prototype._getEndFunction = function(params){
		if(this._slidesHidden){
			this._destroy = function(){
				params.slider._list.children(".zslider-jump-hidden").removeClass("zslider-jump-hidden").show();
			};
			return function(){
				params.slider._list.children(".zslider-jump-hidden").removeClass("zslider-jump-hidden").show();
				params.endFunction();
			}
		} else {
			return ListSlideChanger.prototype._getEndFunction.apply(this, arguments);
		}
	};
	
	function JqueryListAnimation(params){
		this._speed = params.sliderSpeed;
		this._easing = params.easing;
	}
	JqueryListAnimation.prototype.animate = function(element, cssProperty, value, endFunction){
		this._elem = element;
		this._endFunction = endFunction;
		var motionProperties = {};
		motionProperties[cssProperty] = value;
		element.animate(
			motionProperties,
			this._speed,
			this._easing,
			endFunction
		);
	};
	JqueryListAnimation.prototype.destroy = function(){
		this._elem.stop(true, true);
	}
	
	function NoListAnimation(){}
	NoListAnimation.prototype.animate = function(element, cssProperty, value, endFunction){
		endFunction();
	};
	NoListAnimation.prototype.destroy = function(){}

	function SlideBySlideChanger(animation){
		this._animation = animation;
	};
	SlideBySlideChanger.prototype.CLONE_SLIDE_CLASS = "zslides-slide-clone-animation";
	SlideBySlideChanger.prototype.startChanging = function(params){
		this._startSlides = [];
		this._endSlides = [];
		
		this._initHtmlElements(params);
		
		this._destroy = this._getEndFunction(params);
		
		var thisChanger = this;
		var endFunction = function(){
			thisChanger._destroy();
			params.endFunction();
		}
		
		this._animation.animate(
			this._startSlides,
			this._endSlides,
			endFunction
		);
	};
	SlideBySlideChanger.prototype._initHtmlElements = function(params){
		var slideClone;
		var slide;
		var slidePosition;
		var i;
		for(i = 0; i < params.slider._shownCount; i++){
			//clone start slides
			slide = params.slider._getSlide(params.slider.getCurrentSlide() + i);
			if(slide.length > 0){
				slidePosition = slide.position();
				if(slide.css("visibility") != "hidden"){
					//hide primary slides
					slide.css("visibility", "hidden");
					
					slideClone = slide.clone(false).addClass(this.CLONE_SLIDE_CLASS);
					this._startSlides.push(slideClone);
					//place clones
					slidePosition = slide.position();
					slideClone.css(slidePosition);
					slideClone.css("visibility", "visible");
				}
			} else {
				//calc a position by the previous slide's position
				var firstPosition = params.slider._getSlide(0).position();
				var nextPosition = params.slider._getSlide(1).position();
				var newPosition = {};
				newPosition.left = slidePosition.left + (nextPosition.left - firstPosition.left);
				newPosition.top = slidePosition.top + (nextPosition.top - firstPosition.top);
				slidePosition = newPosition;
			}
			
			//clone end slides
			slideClone = params.slider._getSlide(params.index + i).clone(false);
			if((slideClone.length > 0) && (!slideClone.hasClass("zslider-invisible-slide"))){
				slideClone.addClass(this.CLONE_SLIDE_CLASS);
				this._endSlides.push(slideClone);
				//place clone
				slideClone.css(slidePosition);
				slideClone.css("visibility", "visible");
				slideClone.hide();
			}
		}
		for(i = 0; i < this._startSlides.length; i++){
			this._startSlides[i].appendTo(params.slider._list);
		}
		for(i = 0; i < this._endSlides.length; i++){
			this._endSlides[i].appendTo(params.slider._list);
		}
	}
	SlideBySlideChanger.prototype._getEndFunction = function(params){
		var thisChanger = this;
		return function(){
			for(var i = 0; i < params.slider._shownCount; i++){
				params.slider._getSlide(params.slider.getCurrentSlide() + i).prop('style').removeProperty("visibility");
			}
			params.slider._list.children("." + thisChanger.CLONE_SLIDE_CLASS).remove();
		};
	}
	SlideBySlideChanger.prototype.destroy = function(){
		if(typeof this._animation.destroy !== 'undefined'){
			this._animation.destroy();
		}
		if(typeof this._destroy !== 'undefined'){
			this._destroy();
		}
	}
	
	function SlideBySlideOverflowChanger(animation){
		SlideBySlideChanger.apply(this, arguments);
	};
	SlideBySlideOverflowChanger.prototype = Object.create(SlideBySlideChanger.prototype);
	SlideBySlideOverflowChanger.prototype._initHtmlElements = function(params){
		var slideClone;
		var slide;
		var startSlidePosition;
		
		var containerWidth = params.slider._list.parent().width();
		var containerHeight = params.slider._list.parent().height();
		
		var clonesList = params.slider._list.clone(false);
		clonesList.empty();
		params.slider._list.parent().append(clonesList);
		params.slider._setSize(clonesList, params.slider._slideDisplacement * params.slider._shownCount - params.slider._margin);
		params.slider._moveAnyList(clonesList, 0);
		
		//hide main list
		params.slider._list.hide();
		//container sizes
		this._containerCssOldWidth = params.slider._list.parent().prop("style").width;
		this._containerCssOldHeight = params.slider._list.parent().prop("style").height;
		params.slider._list.parent().width(containerWidth);
		params.slider._list.parent().height(containerHeight);
		params.slider._list.parent().css("overflow", "visible");
		var i;
		var invisibleSlideClone;
		for(i = 0; i < params.slider._shownCount; i++){
			//clone start slides
			slide = params.slider._getSlide(params.slider.getCurrentSlide() + i);
			
			if(slide.length > 0){
				invisibleSlideClone = slide.clone(false);
			} else {
				invisibleSlideClone = invisibleSlideClone.clone(false);
			}
			clonesList.append(invisibleSlideClone);
			invisibleSlideClone.css("visibility", "hidden");
			if(i == (params.slider._shownCount - 1)){
				invisibleSlideClone.css(params.slider._elementsMarginParam, "0px");
			}
			startSlidePosition = invisibleSlideClone.position();
			
			if((slide.length > 0) && (slide.css("visibility") != "hidden")){
				slideClone = slide.clone(false).addClass(this.CLONE_SLIDE_CLASS);
				slideClone.css(startSlidePosition);
				slideClone.css(params.slider._elementsMarginParam, "0px");
				this._startSlides.push(slideClone);
			}
			
			//clone end slides
			slideClone = params.slider._getSlide(params.index + i).clone(false);
			if((slideClone.length > 0) && (!slideClone.hasClass("zslider-invisible-slide"))){
				slideClone.addClass(this.CLONE_SLIDE_CLASS);
				this._endSlides.push(slideClone);
				//place clone
				slideClone.css(startSlidePosition);
				slideClone.css(params.slider._elementsMarginParam, "0px");
				slideClone.hide();
			}
		}
		for(i = 0; i < this._startSlides.length; i++){
			clonesList.append(this._startSlides[i]);
		}
		for(i = 0; i < this._endSlides.length; i++){
			clonesList.append(this._endSlides[i]);
		}
		this._clonesList = clonesList;
	}
	SlideBySlideOverflowChanger.prototype._getEndFunction = function(params){
		var thisChanger = this;
		return function(){
			thisChanger._clonesList.remove();
			params.slider._list.show();
			params.slider._list.parent().prop('style').removeProperty('overflow');
			params.slider._list.parent().css("width", thisChanger._containerCssOldWidth);
			params.slider._list.parent().css("height", thisChanger._containerCssOldHeight);
		};
	}
	
	function SlideBySlideAnimation(params){
		this._startSlideAnimation = params.prevSlideAnimation || function(slide, endFunction){slide.hide(), endFunction(); };
		this._endSlideAnimation = params.nextSlideAnimation || function(slide, endFunction){ slide.show(), endFunction(); };
		this._slideInterval = typeof params.slideInterval !== 'undefined' ? params.slideInterval : 0;
		this._startEndInterval = typeof params.prevNextInterval !== 'undefined' ? params.prevNextInterval : 0;
		this._sliderSpeed = typeof params.sliderSpeed !== 'undefined' ? params.sliderSpeed : 0;
		if( (this._slideInterval == 0) && (this._startEndInterval == 0) ){
			this._needDelay = false;
		} else {
			this._needDelay = true;
		}
	};
	SlideBySlideAnimation.prototype.animate = function(startSlides, endSlides, endFunction){
		var shownCount = 0;
		var count = startSlides.length + endSlides.length;
		this._isDestroyed = false;
		
		var thisAnimation = this;
		var checkEndFunction = function(){
			shownCount++;
			if( (shownCount == count) && !thisAnimation._isDestroyed){
				endFunction();
			}
		};
		var getStartFunction = function(slideNum){
			return function(){
				thisAnimation._startSlideAnimation(startSlides[slideNum], thisAnimation._sliderSpeed, checkEndFunction);
			}
		}
		var getEndFunction = function(slideNum){
			return function(){
				thisAnimation._endSlideAnimation(endSlides[slideNum], thisAnimation._sliderSpeed - thisAnimation._startEndInterval, checkEndFunction);
			}
		}
		
		this._timeouts = [];
		var i;
		for(i = 0; i < startSlides.length; i++){
			if(this._needDelay){
				this._timeouts.push(setTimeout(
					getStartFunction(i),
					this._slideInterval * i
				));
			} else {
				this._startSlideAnimation(startSlides[i], this._sliderSpeed, checkEndFunction);
			}
		}
		
		for(i = 0; i < endSlides.length; i++){
			if(this._needDelay){
				this._timeouts.push(setTimeout(
					getEndFunction(i),
					this._slideInterval * i + this._startEndInterval
				));
			} else {
				this._endSlideAnimation(endSlides[i], this._sliderSpeed - this._startEndInterval, checkEndFunction);
			}
		}
	};
	SlideBySlideAnimation.prototype.destroy = function(){
		this._isDestroyed = true;
		for(var i = 0; i < this._timeouts.length; i++){
			clearTimeout(this._timeouts[i]);
		}
	}
	
	function ObjectStorage(id){
		this._maxIndex = -1;
		this._storage = [];
		this._id = id;
	}
	ObjectStorage.prototype.add = function(elem, object){
		this._storage[this._getIndex(elem)] = object;
	}
	ObjectStorage.prototype.get = function(elem){
		return this._storage[this._getIndex(elem)];
	}
	ObjectStorage.prototype.remove = function(elem){
		this._storage[this._getIndex(elem)] = undefined;
		this._deleteIndex(elem);
	}
	ObjectStorage.prototype._getIndex = function(elem){
		if( typeof elem.data(this._id) === "undefined" ){
			elem.data(this._id, this._newIndex())
		}
		return elem.data(this._id);
	}
	ObjectStorage.prototype._newIndex = function(){
		this._maxIndex++;
		return this._maxIndex;
	}
	ObjectStorage.prototype._deleteIndex = function(elem){
		elem.removeData(this._id);
	}
	
	function SlideLinks(params){
		this._slidesLinks = params.slidesLinks;
		this._slidesLinksSelectedClass = params.slidesLinksSelectedClass;
		this._slidesLinksOut = params.slidesLinksOut;
		this._showSlideFunction = params.showSlideFunction;
		
		var _thisLinksObject = this;
		this._showSlideFunctionInner = function(e){
			e.preventDefault();
			if(!$(this).hasClass(_thisLinksObject._slidesLinksSelectedClass)){
				var linkIndex = parseInt($(this).attr(_thisLinksObject.LINK_INDEX_PARAM_NAME));
				_thisLinksObject._showSlideFunction(linkIndex);
			}
		}
		
		var linkIndex = 0;
		$(this._slidesLinks).each(function(){
			$(this).attr(_thisLinksObject.LINK_INDEX_PARAM_NAME, linkIndex);
			$(this).on("click", _thisLinksObject._showSlideFunctionInner);
			if(_thisLinksObject._slidesLinksOut){
				$(this).on("mouseenter", _thisLinksObject._showSlideFunctionInner);
			}
			linkIndex++;
		});
		
		this.highlightLink(0);
	}
	SlideLinks.prototype.LINK_INDEX_PARAM_NAME = "data-zslider-link-index";
	SlideLinks.prototype.highlightLink = function(index){
		$(this._slidesLinks).removeClass(this._slidesLinksSelectedClass);
		this._getLinkBySlideIndex(index).addClass(this._slidesLinksSelectedClass);
	}
	SlideLinks.prototype._getLinkBySlideIndex = function(index){
		return $(this._slidesLinks).eq(index);
	}
	SlideLinks.prototype.destroy = function(){
		var _thisLinksObject = this;
		$(this._slidesLinks).each(function(){
			$(this).unbind("click", _thisLinksObject._showSlideFunctionInner);
			if(_thisLinksObject._slidesLinksOut){
				$(this).unbind("mouseenter", _thisLinksObject._showSlideFunctionInner);
			}
			$(this).removeClass(_thisLinksObject._slidesLinksSelectedClass);
			$(this).removeAttr(_thisLinksObject.LINK_INDEX_PARAM_NAME);
		});
	}
	
	function SliderAnimationController(slider){
		this._slider = slider;
		this._nextShowSlideFunction = null;
		this._needResize = false;
		this._disabled = false;
		var thisConttroller = this;
		this._onSlideChange = function(){
			thisConttroller._showSlideAfterChange();
		};
		this._slider.getParent().on('slidechange', this._onSlideChange);
	}
	SliderAnimationController.prototype.showSlide = function(index){
		if(this._disabled){
			return;
		}
		this._setShowNextFunction(function(){ this._slider.showSlide(index); });
	}
	SliderAnimationController.prototype.setCurrentSlide = function(index){
		if(this._disabled){
			return;
		}
		this._setShowNextFunction(function(){ this._slider.setCurrentSlide(index); });
	}
	SliderAnimationController.prototype.resizeSlides = function(){
		if(!this.isMoving() && !this._disabled){
			this._slider.resizeSlides();
			this._needResize = false;
		} else {
			this._needResize = true;
		}
	}
	SliderAnimationController.prototype.showNext = function(count){
		if(this._disabled){
			return;
		}
		this._setShowNextFunction(function(){ this._slider.showNext(count); });
	}
	SliderAnimationController.prototype.showPrev = function(count){
		if(this._disabled){
			return;
		}
		this._setShowNextFunction(function(){ this._slider.showPrev(count); });
	}
	SliderAnimationController.prototype.isMoving = function(){
		return this._slider.isMoving();
	}
	SliderAnimationController.prototype._setShowNextFunction = function(showSlideFunction){
		if(!this.isMoving()){
			showSlideFunction.apply(this);
			this._nextShowSlideFunction = null;
		} else {
			this._nextShowSlideFunction = showSlideFunction;
		}
	}
	SliderAnimationController.prototype._showSlideAfterChange = function(){
		if(this._needResize){
			this._slider.resizeSlides();
			this._needResize = false;
		}
		if(this._nextShowSlideFunction !== null){
			var nextFunction = this._nextShowSlideFunction;
			this._nextShowSlideFunction = null;
			nextFunction.apply(this);
		}
	}
	SliderAnimationController.prototype.destroy = function(){
		this._slider.getParent().off('slidechange', this._onSlideChange);
		this._onSlideChange = null;
		this._slider = null;
		this._nextShowSlideFunction = null;
	}
	SliderAnimationController.prototype.getSlider = function(){
		return this._slider;
	}
	SliderAnimationController.prototype.isDisabled = function(){
		return this._disabled;
	}
	SliderAnimationController.prototype.tryDisable = function(){
		if(this.isMoving() || this._disabled || this._slider.isDisabled()){
			return false;
		}
		this._disabled = true;
		return true;
	}
	SliderAnimationController.prototype.enable = function(){
		if(this.isMoving()){
			return;
		}
		if(this._needResize){
			this._slider.resizeSlides();
			this._needResize = false;
		}
		this._disabled = false;
	}
	
	function ArrowLink(params){
		this._arrow = params.arrow;
		this._isActive = false;
		this._inactiveClass = params.inactiveClass;
		this._onClickOuter = params.onClick;
		
		var thisLink = this;
		this._onClick = function(e){
			e.preventDefault();
			if(thisLink._isActive) thisLink._onClickOuter(e);
		}
		$(this._arrow).on('click', this._onClick);
	}
	ArrowLink.prototype.destroy = function(){
		$(this._arrow).off('click', this._onClick);
	}
	ArrowLink.prototype.setActive = function(isActive){
		this._isActive = isActive;
		if(this._isActive){
			$(this._arrow).removeClass(this._inactiveClass);
		} else {
			$(this._arrow).addClass(this._inactiveClass);
		}
	}
	
	function EventHandlersStorage(){
		this._events = [];
		this._handlers = [];
		this._elements = []
	}
	EventHandlersStorage.prototype.add = function(elem, event, handler){
		this._elements.push(elem);
		this._events.push(event);
		this._handlers.push(handler);
		$(elem).on(event, handler);
	}
	EventHandlersStorage.prototype.destroy = function(){
		for(var i = 0; i < this._events.length; i++){
			$(this._elements[i]).off(this._events[i], this._handlers[i]);
		}
	}
	
	function Timer(interval, functionToRun){
		this._timeout = null;
		this._active = true;
		this._functionToRun = functionToRun;
		this._interval = interval;
	}
	Timer.prototype.start = function(){
		this.stop();
		if(this._active){
			this._timeout = setTimeout(this._functionToRun, this._interval);
		}
	}
	Timer.prototype.stop = function(){
		clearTimeout(this._timeout);
	}
	Timer.prototype.destroy = function(){
		this.stop();
	}
	Timer.prototype.on = function(){
		if(this._active){
			return;
		}
		this._active = true;
		this.start();
	}
	Timer.prototype.off = function(){
		if(!this._active){
			return;
		}
		this._active = false;
		this.stop();
	}
	
	function AdditionalSlidesController(list, shownCount, moveCount){
		this._list = list;
		var slidesCount = this._list.children("li").length;
		if( (slidesCount > shownCount) && (slidesCount % moveCount != 0 ) ){
			var newSlidesCount = moveCount - (slidesCount % moveCount);
			var newSlide;
			for(var i = 0; i < newSlidesCount; i++){
				newSlide = this._list.children("li").eq(0).clone(false);
				newSlide.addClass("zslider-invisible-slide");
				newSlide.appendTo(this._list);
			}
		}
	}
	AdditionalSlidesController.prototype.destroy = function(){
		this._list.children(".zslider-invisible-slide").remove();
	}
	
	function MoveTouchController(element, params){
		this._element = element;
		this._isMoving = false;
		
		this._allowVerticalMoving = params.allowVerticalMoving === undefined ? true : params.allowVerticalMoving;
		this._allowHorizontalMoving = params.allowHorizontalMoving === undefined ? true : params.allowHorizontalMoving;
		this._propertyY = params.propertyY === undefined ? "margin-top" : params.propertyY;
		this._propertyX = params.propertyX === undefined ? "margin-left" : params.propertyX;
		this._touchTimeLimit = params.touchTimeLimit === undefined ? 10 : params.touchTimeLimit;
		this._touchPositionX = 0;
		this._touchPositionY = 0;
		this._distanceX = 0;
		this._distanceY = 0;
		this._time = 0;
		this._touchTime = 0;
		
		var thisController = this;
		this._element.on("touchstart", function(e){
			e.preventDefault();
			thisController._startMoving(e);
		});
		this._element.on("touchmove", function(e){
			e.preventDefault();
			thisController._moving(e);
		});
		this._element.on("touchend", function(e){
			e.preventDefault();
			thisController._stopMoving(e);
		});
	}
	MoveTouchController.prototype._startMoving = function(e){
		if(this._raiseEvent("touchControllerStart")){
			this._isMoving = true;
			this._touchPositionX = this._getEventPositionX(e);
			this._touchPositionY = this._getEventPositionY(e);
			this._touchTime = (new Date()).getTime();
		}
	}
	MoveTouchController.prototype._raiseEvent = function(eventName, data){
		var event = jQuery.Event(eventName);
		this._element.triggerHandler(event, data);
		return !event.isDefaultPrevented();
	}
	MoveTouchController.prototype._moving = function(e){
		if(!this._isMoving){
			return;
		}
		var currentTouchTime = (new Date()).getTime();
		this._time = currentTouchTime - this._touchTime;
		this._touchTime = currentTouchTime;
		
		if(this._allowHorizontalMoving){
			var touchX = this._getEventPositionX(e);
			this._distanceX = (this._touchPositionX - touchX);
			this._setPositionX(this._getPositionX() - this._distanceX);
			this._touchPositionX = touchX;
		}
		
		if(this._allowVerticalMoving){
			var touchY = this._getEventPositionY(e);
			this._distanceY = (this._touchPositionY - touchY);
			this._setPositionY(this._getPositionY() - this._distanceY);
			this._touchPositionY = touchY;
		}
	}
	MoveTouchController.prototype._stopMoving = function(e){
		if(!this._isMoving){
			return;
		}
		this._isMoving = false;
		
		var lastMoveTime = (new Date()).getTime() - this._touchTime;
		if(lastMoveTime > this._touchTimeLimit){
			this._distanceX = 0;
			this._distanceY = 0;
		} else if(lastMoveTime == 0){
			lastMoveTime = 1;
		}
		
		this._raiseEvent(
			"touchControllerStop",
			{
				speedX: this._distanceX / lastMoveTime,
				speedY: this._distanceY / lastMoveTime,
				positionX: this._getPositionX(),
				positionY: this._getPositionY()
			}
		);
	}
	MoveTouchController.prototype._getEventPositionX = function(e){
		return e.originalEvent.changedTouches[0].clientX;
	}
	MoveTouchController.prototype._getEventPositionY = function(e){
		return e.originalEvent.changedTouches[0].clientY;
	}
	MoveTouchController.prototype._getCssProperty = function(propertyName){
		var cssProperty = this._element.css(propertyName);
		if(cssProperty === undefined){
			return 0;
		} else {
			return parseInt(cssProperty.replace("px", ""));
		}
	}
	MoveTouchController.prototype._getPositionX = function(){
		return this._getCssProperty(this._propertyX);
	}
	MoveTouchController.prototype._getPositionY = function(){
		return this._getCssProperty(this._propertyY);
	}
	MoveTouchController.prototype._setPositionX = function(position){
		this._element.css(this._propertyX, position + "px");
	}
	MoveTouchController.prototype._setPositionY = function(position){
		this._element.css(this._propertyY, position + "px");
	}
	MoveTouchController.prototype.destroy = function(e){
		this._element.off("touchstart");
		this._element.off("touchmove");
		this._element.off("touchend");
	}
	
	jQuery.extend(
		jQuery.easing,
		{
			zEasing: function (a){
				var t = (1 - a);
				t = t*t;
				return 1 - t*t;
			}
		}
	);

})( jQuery, window, document );